# Use the base Squash TM image
FROM squashtest/squash-tm:5.1.0

# Set the working directory
WORKDIR /opt/squash-tm

# Add the plugins to the appropriate directories
COPY resources/automation.result.publisher.community-5.0.0.RELEASE.jar /opt/squash-tm/plugins
COPY resources/automation.scm.git-5.0.0.RELEASE.jar /opt/squash-tm/plugins
COPY resources/org.eclipse.jgit-6.4.0.202211300538-r.jar /opt/squash-tm/plugins
COPY resources/automation.squashautom.community-5.0.0.RELEASE.jar /opt/squash-tm/plugins
COPY resources/automation.testplan.retriever.community-5.0.0.RELEASE.jar /opt/squash-tm/plugins

### BUILD IMAGE
# docker build -t tomtomgt/squash-autom-devops:1.0 .
# docker push tomtomgt/squash-autom-devops:1.0


### CHECK IMAGE
# cd 'C:\projet_tom\gitlab_ci\squash_deploy'
# docker image ls
# docker image inspect tomtomgt/squash-autom-devops:1.0

### RUN IMAGE
# docker run -it --rm tomtomgt/squash-autom-devops:1.0 /bin/bash
# ls plugins

### RUN IMAGE
# docker run -d --name='squash-tm' -it -p 8090:8080 tomtomgt/squash-autom-devops:1.0
# docker logs -f squash-tm

### STOP IMAGE
# docker stop squash-tm
# docker rm squash-tm
# docker rmi tomtomgt/squash-autom-devops:1.0

### Connect to Squash
# http://localhost:8090/squash

### Check plugin installation
# http://localhost:8090/squash/administration-workspace/servers/test-automation-servers
# => Administration => Server => Test Automation Server => add plugins (see if you can access the new plugins)